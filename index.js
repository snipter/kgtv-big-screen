var ref = require('ref');
var ffi = require('ffi');

var elsa = ffi.Library('ELSA', {
	'INF_GetVersion': [
		'DWORD'
	]
});

console.log(elsa.INF_GetVersion());